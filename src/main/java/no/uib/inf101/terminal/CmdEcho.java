package no.uib.inf101.terminal;

public class CmdEcho implements Command {
    @Override
    public String run(String[] args) {
        return String.join(" ", args); 
    }

    @Override
    public String getName() {
        return "echo"; 
    }
}